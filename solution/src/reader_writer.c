/// @file
/// @brief File reader-writer

#include "reader_writer.h"
#include "pe_file.h"
#include <stdio.h>
#include <stdlib.h>

//interface functions

/// @brief reading
struct Section* pe_file_read_section_from_file(FILE* in, const char* section_name){
    struct PEHeader* peHeader =  pe_file_read_header_from_32_version(in);

    struct PESectionHeader* sectionHeader = pe_file_find_section_header_by_name_from_32_version(in, section_name, peHeader);
    struct Section* section = pe_file_read_section_by_header(in, sectionHeader);

    free(peHeader);
    free(sectionHeader);
    return section;
}

/// @brief 
/// @param out 
/// @param section

// writing
void pe_file_write_section_contents_to_file(FILE* out, const struct Section* section){
    const struct Section* _section = section;
    pe_file_write_section_content_to_bin(out, _section);
}
