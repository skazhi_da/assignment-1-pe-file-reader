/// @file
/// @brief PE file

#include "pe_file.h"
#include <stdint.h>
#include <stdlib.h>
#include <string.h>

//// DOS offset
const uint64_t DOS_OFFSET = 0x3C;
/// Offset to first section
const uint64_t FIRST_SECTION_OFFSET = 0xF0;
/// Offset between two sections
const uint64_t SECTIONS_OFFSET = 0x28;

struct Section{
    uint32_t  SizeOfRawData;
    uint32_t* content;
};

#ifdef _MSC_VER
#pragma packed(push, 1)
#endif
struct
#ifdef __clang__
    __attribute__((packed))
#elif defined __GNUC__
        __attribute__((packed))
#endif
/// @brief containing PE file's header information
PEHeader {
    uint8_t     magic[4];
    uint16_t    machine;
    uint16_t    NumberOfSections;
    uint32_t    TimeDateStamp;
    uint32_t    PointerToSymbolTable;
    uint32_t    NumberOfSymbols;
    uint16_t    SizeOfOptionalHeader;
    uint16_t    Characteristics;
};
#ifdef _MSC_VER
#pragma packed(pop)
#endif

#ifdef _MSC_VER
#pragma packed(push, 1)
#endif
struct
#ifdef __clang__
    __attribute__((packed))
#elif defined __GNUC__
        __attribute__((packed))
#endif
/// @brief containing PE file's data directory information
ImageDataDirectory{
    uint32_t    VirtualAddress;
    uint32_t    Size;
};
#ifdef _MSC_VER
#pragma packed(pop)
#endif

#ifdef _MSC_VER
#pragma packed(push, 1)
#endif
struct
#ifdef __clang__
    __attribute__((packed))
#elif defined __GNUC__
        __attribute__((packed))
#endif
/// @brief containing PE file's optional header information
PEOptionalHeader {
    uint16_t    magic;
    uint8_t     MajorLinkerVersion;
    uint8_t     MinorLinkerVersion;
    uint32_t    SizeOfCode;
    uint32_t    SizeOfInitializedData;
    uint32_t    SizeOfUninitializedData;
    uint32_t    AddressOfEntryPoint;
    uint32_t    BaseOfCode;
    uint32_t    BaseOfData;
    uint32_t    ImageBase;
    uint32_t    SectionAlignment;
    uint32_t    FileAlignment;
    uint16_t    MajorOperatingSystemVersion;
    uint32_t    MinorOperatingSystemVersion;
    uint16_t    MajorImageVersion;
    uint16_t    MinorImageVersion;
    uint16_t    MajorSubsystemVersion;
    uint16_t    MinorSubsystemVersion;
    uint32_t    Win32VersionValue;
    uint32_t    SizeOfImage;
    uint32_t    SizeOfHeaders;
    uint32_t    CheckSum;
    uint16_t    Subsystem;
    uint16_t 	DllCharacteristics;
    uint32_t 	SizeOfStackReserve;
    uint32_t    SizeOfStackCommit;
    uint32_t    SizeOfHeapReserve;
    uint32_t    SizeOfHeapCommit;
    uint32_t    LoaderFlags;
    uint32_t    NumberOfRvaAndSizes;
    uint32_t    DataDirectoryNumber;
    struct ImageDataDirectory *directories;
};
#ifdef _MSC_VER
#pragma packed(pop)
#endif

#ifdef _MSC_VER
#pragma packed(push, 1)
#endif
struct
#ifdef __clang__
    __attribute__((packed))
#elif defined __GNUC__
        __attribute__((packed))
#endif
/// @brief containing PE file's section header information
PESectionHeader {
    uint8_t     SectionName[8];
    uint32_t    PhysicalAddres;
    uint32_t    VirtualSize;
    uint32_t    SizeOfRawData;
    uint32_t    PointerToRawData;
    uint32_t    PointerToRelocations;
    uint32_t    PointerToLineNumbers;
    uint16_t    NumberOfRelocations;
    uint16_t    NumberOfLineNumbers;
    uint32_t    Characteristics;
};
#ifdef _MSC_VER
#pragma packed(pop)
#endif

size_t pe_file_section_get_size(const struct Section* section){
    return sizeof(*section);
}

struct PEHeader* pe_file_read_header_from_32_version(FILE* in){
    struct PEHeader* PEHeader = (struct PEHeader*)malloc(sizeof(struct PEHeader));

    uint32_t offset = 0;
    fseek(in, (long)DOS_OFFSET, SEEK_SET);
    fread(&offset, 4, 1, in);

    fseek(in, offset, SEEK_SET);
    fread(PEHeader, sizeof(struct PEHeader), 1, in);

    return PEHeader;
}

struct PESectionHeader* pe_file_find_section_header_by_name_from_32_version(FILE* in, const char* section_name, struct PEHeader* pe_header){
    uint16_t unchecked = pe_header->NumberOfSections;

    char current_section_name[16] = {0};

    fseek(in, (long)FIRST_SECTION_OFFSET, SEEK_CUR);
    while((strcmp(current_section_name, section_name) != 0) && (unchecked-->0)){
        fread(&current_section_name, 16, 1, in);
        fseek(in, (long)SECTIONS_OFFSET - 16, SEEK_CUR);
    }
    if (strcmp(current_section_name, section_name) == 0){
        struct PESectionHeader* sectionHeader = (struct PESectionHeader*)malloc(sizeof(struct PESectionHeader));

        fseek(in, (long) -SECTIONS_OFFSET, SEEK_CUR);
        fread(sectionHeader, sizeof(struct PESectionHeader), 1, in);
        return sectionHeader;
    }else{
        return NULL;
    }
}

struct Section* pe_file_read_section_by_header(FILE* in, struct PESectionHeader* section_header){
    struct Section* section = (struct Section*)malloc(sizeof(struct Section));
    section->SizeOfRawData = section_header->SizeOfRawData;
    section->content = calloc((size_t) section->SizeOfRawData, 1);
    fseek(in, section_header->PointerToRawData, SEEK_SET);
    fread(section->content, section_header->SizeOfRawData, 1, in);
    return section;
}

void pe_file_write_section_content_to_bin(FILE* out, const struct Section* section){
    fwrite(section->content, section->SizeOfRawData, 1, out);
}

void pe_file_section_destroy(struct Section* section){
    free(section->content);
    free(section);
}
