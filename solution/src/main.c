/// @file
/// @brief Main application file

#include "reader_writer.h"
#include "pe_file.h"
#include <stdio.h>

/// Application name string
#define APP_NAME "section-extractor"

/// @brief Print usage test
/// @param[in] f File to print to (e.g., stdout)
void usage(FILE *f)
{
    fprintf(f, "Usage: " APP_NAME " <in_file> <section_name> <out_file>\n");
}

/// @brief Application entry point
/// @param[in] argc Number of command line arguments
/// @param[in] argv Command line arguments
/// @return 0 in case of success or error code
int main(int argc, char** argv)
{
    (void) argc; (void) argv; // suppress 'unused parameters' warning
    if (argc != 4) {usage(stdout); return -1;}

    const char* in_file_name    = argv[1];
    const char* out_file_name   = argv[3];
    const char* section_name    = argv[2];

    FILE * in_file  = fopen(in_file_name, "rb");
    FILE * out_file  = fopen(out_file_name, "wb");

    struct Section* section = pe_file_read_section_from_file(in_file, section_name);
    pe_file_write_section_contents_to_file(out_file, section);

    fclose(in_file);
    fclose(out_file);

    pe_file_section_destroy(section);

    return 0;
}
