#ifndef FILE_READER_WRITER_HEADER
#define FILE_READER_WRITER_HEADER

#include "pe_file.h"
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>

struct Section* pe_file_read_section_from_file(FILE* in, const char* section);
void pe_file_write_section_contents_to_file(FILE* out, const struct Section* section);

#endif
